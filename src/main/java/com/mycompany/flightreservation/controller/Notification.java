/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.controller;

/**
 *
 * @author taohuh
 */
public class Notification extends OptionalService{
    
    private double addPrice = 100;
    
    public Notification(FlightReservationItem decoratedFlight){
        super(decoratedFlight);
        System.out.println("Adding Notofication Service");
    }
   
    public Double getPrice() {
        return decoratedFlight.getPrice() + addPrice;
    }

    @Override
    public String getSeat() {
        return decoratedFlight.getSeat();
    }

    @Override
    public String getFlightDetail() {
       return decoratedFlight.getFlightDetail() + " , Notification Service";
    }
    
    public double addedPrice(){
        return addPrice;
    }
    
}
