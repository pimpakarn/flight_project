/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.controller;


import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import com.mycompany.flightreservation.controller.MyFlightDetail;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;

public class MyFlightListCellRenderer extends JPanel
        implements ListCellRenderer<MyFlightDetail> {
    private ImageIcon icon ;
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
    }
    
    @Override
    public Component getListCellRendererComponent(JList list, MyFlightDetail value,
            int index, boolean isSelected, boolean cellHasFocus) {

        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        this.removeAll();
        double price = value.getPrice();
        JLabel priceLabel = new JLabel(String.valueOf(price));
        priceLabel.setFont(new Font("Courier New", Font.BOLD, 20));
        priceLabel.setForeground(Color.BLUE);
        priceLabel.setPreferredSize(new Dimension(100, 20));
        priceLabel.setMinimumSize(new Dimension(100, 20));
        priceLabel.setMaximumSize(new Dimension(100, 20));
        
        this.add(priceLabel);

        Component rigidArea1
                = Box.createRigidArea(new Dimension(20, 20));
        this.add(rigidArea1);
        
        String airline = value.getAirlineName();
        System.out.println(airline);
        icon = new ImageIcon("src\\resources\\" + airline+"thai.png");
        JLabel airlineLabel = new JLabel(airline, icon, SwingConstants.LEADING);
        this.add(airlineLabel);
        return this;

    }
}