/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.controller;

/**
 *
 * @author taohuh
 */
public class MyFlightDetail{

    public static String airlineName;
    public static Double price;
    
    public MyFlightDetail(String airlineName, double price){
        this.airlineName = airlineName;
        this.price = price;
    }
    public void setAirlineName(String AirlineName){
        this.airlineName = AirlineName;
    }
    public String getAirlineName(){
        return airlineName;
    }
    
    public void setPrice(Double p){
        this.price = p;
    }
    public Double getPrice(){
        return price;
    }
    
}
