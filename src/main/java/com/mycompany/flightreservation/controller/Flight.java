/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.flightreservation.controller;

/**
 *
 * @author CSTURoom111
 */
public class Flight {
    public static void main(String[] args) {
        FlightReservationItem flightReservationItem = new PlainFlightTicket(2055, "1", "One-way");
        FlightReservationItem flightReservationItem2 = new Insurance(flightReservationItem);
        System.out.println(flightReservationItem2.getPrice() + " Bath");
        System.out.println(flightReservationItem2.getSeat());
        System.out.println(flightReservationItem2.getFlightDetail());
        
        FlightReservationItem flightReservationItem3 = new Notification(flightReservationItem2);
        System.out.println(flightReservationItem3.getPrice() + " Bath");
        System.out.println(flightReservationItem3.getSeat());
        System.out.println(flightReservationItem3.getFlightDetail());
    }
}
