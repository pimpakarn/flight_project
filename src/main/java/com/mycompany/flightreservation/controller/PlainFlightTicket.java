/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.controller;

/**
 *
 * @author taohuh
 */
    public class PlainFlightTicket implements FlightReservationItem {
        
        private double price;
        private String seat;
        private String flightDetail;
        
        public PlainFlightTicket(double price, String seat, String flightDetail){
            this.price = price;
            this.seat = seat;
            this.flightDetail = flightDetail;
        }
        
        public Double getPrice() {
            return price;
        }

        public String getSeat() {
            return seat + " Seat";
        }

        public String getFlightDetail() {
            return flightDetail;
        }
    
}
