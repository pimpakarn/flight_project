/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.controller;

/**
 *
 * @author taohuh
 */
public class Insurance extends OptionalService{
    private double addPrice = 10;
    
    public Insurance(FlightReservationItem decoratedFlight){
        super(decoratedFlight);
        System.out.println("Adding Insurance Service");
    }
    
    public String getFlightDetail(){
        return decoratedFlight.getFlightDetail() + " , Insurance Service";
    }
    
    public Double getPrice(){
        return decoratedFlight.getPrice() + addPrice;
    }

    /**
     *
     * @return
     */
    @Override
    public String getSeat(){
        return decoratedFlight.getSeat();
    }
    
    public double getAddedPrice(){
        return addPrice;
    }
}
